﻿using UnityEngine;
using System.Collections;

public class AmmoView : SpaceImpactElement {

	public int id;

	public override void Initialize () {
		App.view.ammoList.Add(this);
	}

	public override void Update () {
		transform.position = App.model.ammoList[id].position;
	}

	void OnCollisionEnter2D () {
		if(!App.controller.ammoList[id].isActive) return;
		App.controller.ammoList[id].Notify(Constants.Hit);
	}
}
