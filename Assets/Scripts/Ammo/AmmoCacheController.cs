﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AmmoCacheController : SpaceImpactElement {

	[SerializeField] AmmoView ammoViewPrefab;
	[SerializeField] AmmoController ammoControllerPrefab;
	[SerializeField] int initialCount;

	public override void Initialize () {
		App.controller.ammoCache = this;
//		WarmUp(initialCount);
	}

	void WarmUp (int count) {
		for(int i = 0; i < count; i++) {
			CreateAmmo(i);
		}
	}

	void CreateAmmo (int id) {
		App.model.ammoList.Add(new AmmoModel(id));
		
		AmmoView ammoViewInstance = Instantiate(ammoViewPrefab) as AmmoView;
		App.view.ammoList.Add(ammoViewInstance);
		ammoViewInstance.id = id;
		ammoViewInstance.transform.SetParent(App.view.ammoCache.transform);

		AmmoController ammoControllerInstance = Instantiate(ammoControllerPrefab) as AmmoController;
		App.controller.ammoList.Add(ammoControllerInstance);
		ammoControllerInstance.id = id;
		ammoControllerInstance.transform.SetParent(App.controller.ammoCache.transform);
//		ammoControllerInstance.SetActive(false);
	}

	public void OnRequestAmmo () {
		if(App.controller.ammoList == null || App.controller.ammoList.Count <= 0) {
			WarmUp(initialCount);
		}

		for(int i = 0; i < App.controller.ammoList.Count; i++) {
			AmmoController ammoController = App.controller.ammoList[i].GetComponent<AmmoController>();
			if(!ammoController.isActive) {
				ammoController.SetActive(true);
				return;
			}
		}
	}
}
