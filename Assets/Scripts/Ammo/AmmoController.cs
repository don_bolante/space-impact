﻿using UnityEngine;
using System.Collections;

public class AmmoController : SpaceImpactElement {

	public int id;

	public bool isActive;
	[SerializeField] int direction;
	[SerializeField] float moveSpeed;
	[SerializeField] Vector3 spawnPointOffset;

	public override void Initialize () {
		App.controller.ammoList.Add(this);
	}

	public override void Update () {
		if(!isActive) return;
		App.model.ammoList[id].position += Vector3.right * direction * moveSpeed * Time.deltaTime;

		if(!StaticHelper.IsVisible(App.view.ammoList[id].transform)) {
			SetActive(false);
		}
	}

	public void Notify (string notification) {
		switch(notification) {
		case Constants.Hit:
			SetActive(false);
			break;
		}
	}

	public void SetActive (bool flag) {
		if(flag) {
			App.model.ammoList[id].position = App.view.hero.transform.position + spawnPointOffset;
			isActive = true; 
		}
		else {
			App.model.ammoList[id].position = Vector3.one * 500;
			isActive = false;
		}
	}
}
