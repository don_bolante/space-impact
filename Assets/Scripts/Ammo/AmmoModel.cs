﻿using UnityEngine;
using System.Collections;

public class AmmoModel {

	public AmmoModel () {}
	public AmmoModel (int id) {
		this.id = id;
		position = Vector3.one * 5000;
	}

	public int id;
	public Vector3 position;
}
