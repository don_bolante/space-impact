﻿using UnityEngine;
using System.Collections;

public class HeroController : SpaceImpactElement, IUnitAction {

	[SerializeField] public Sprite[] ship;

	public override void Update () {

		Move (Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

		if(Input.GetKeyUp(KeyCode.Space)) {
			Fire();
		}
	}

	public override void Initialize () {
		App.controller.hero = this;
	}

	public void Notify (string command, GameObject agent) {
		switch(command) {
		case Constants.Hit:
			switch(agent.tag) {
			case Constants.Enemy:
				if(App.model.hero.state == HeroState.None) {
					if(App.model.lives - 1 <= 0) {
						App.controller.Notify(Constants.GameOver);
					}
					App.model.lives--;
					App.controller.hud.Notify(Constants.Update);
					App.controller.heroStateMachine.ChangeState(HeroState.Invulnerable);
					App.model.hero.position = App.model.hero.spawnPoint;
				}
				break;
			case Constants.Powerup:

				break;
			default:
				// Do nothing.
				break;
			}
			break;
		}
	}

	public void Move (float horizontal, float vertical) {
		Vector2 movePosition = new Vector2(horizontal, vertical) * Time.deltaTime * App.model.hero.moveSpeed;
		App.model.hero.position += new Vector3(movePosition.x, movePosition.y);

		// Restricts the ship position inside the screen
		Vector3 currentPosition = App.model.hero.position;
		Transform heroTransform = App.view.hero.transform;
		currentPosition.x = Mathf.Clamp(currentPosition.x, StaticHelper.GetRestrictedPosition(heroTransform).x, 
		                            					    -StaticHelper.GetRestrictedPosition(heroTransform).x);
		currentPosition.y = Mathf.Clamp(currentPosition.y, StaticHelper.GetRestrictedPosition(heroTransform).y, 
		                                					-StaticHelper.GetRestrictedPosition(heroTransform).y);
		App.model.hero.position = currentPosition;
	}

	public void Fire () {
		App.controller.ammoCache.OnRequestAmmo();
	}
}
