﻿using UnityEngine;
using System.Collections;

public class HeroStateMachine : SpaceImpactElement {

	#region Cooldown properties
	float duration = 3.0f;
	float timeElapsed = 0.0f;
	#endregion
	
	public override void Initialize () {
		App.controller.heroStateMachine = this;
	}

	public override void Update () {
		if(App.model.hero.state == HeroState.None) return;

		timeElapsed += Time.deltaTime;
		if(timeElapsed >= duration) {
			ChangeState(HeroState.None);
			timeElapsed = 0;
		}
	}

	public void ChangeState (HeroState state) {
		App.model.hero.state = state;
		SpriteRenderer spriteRenderer = App.view.hero.GetComponent<SpriteRenderer>();
		spriteRenderer.sprite = App.controller.hero.ship[(int)state];
		BoxCollider2D collider = App.view.hero.GetComponent<BoxCollider2D>();
		collider.size = new Vector2(spriteRenderer.bounds.size.x, spriteRenderer.bounds.size.y);
	}
}
