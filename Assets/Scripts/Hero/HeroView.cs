﻿using UnityEngine;
using System.Collections;

public class HeroView : SpaceImpactElement {

	public override void Update ()
	{
		transform.position = App.model.hero.position;
	}
	
	public override void Initialize () {
		App.view.hero = this;
	}

	void OnCollisionEnter2D (Collision2D collision) {
		App.controller.hero.Notify(Constants.Hit, collision.gameObject);
	}
}
