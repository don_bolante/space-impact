﻿using UnityEngine;
using System.Collections;

public interface IUnitAction {

	void Move(float horizontal, float vertical);

}
