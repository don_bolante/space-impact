﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class HeroModel {

	public int health;
	public int score;
	//public Powerup powerup;
	public HeroState state;

	public Vector3 spawnPoint = new Vector3(2.75f, 0);
	public Vector3 position;
	public int moveSpeed;
	public int attackSpeed;
	public int damage;

	public HeroModel () {
		health = 3;
//		damage = 1;
	}

}
