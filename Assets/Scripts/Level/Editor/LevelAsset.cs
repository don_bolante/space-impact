﻿using UnityEngine;
using UnityEditor;
using System;

public class LevelAsset {

	[MenuItem("Assets/Create/Level Manager")]
	public static void CreateAsset () {
		CustomAssetUtility.CreateAsset<LevelData>();
	}
}
