﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections;

[CustomEditor(typeof(LevelData))]
public class LevelDataEditor : Editor {

	private ReorderableList list;

	private void OnEnable () {
		list = new ReorderableList(serializedObject, serializedObject.FindProperty("list"), true, true, true, true);

		list.drawHeaderCallback = (Rect rect) => {  
			EditorGUI.LabelField(rect, "Enemy Wave (Time, Count, Type)");
		};

		list.drawElementCallback =  
		(Rect rect, int index, bool isActive, bool isFocused) => {
			var element = list.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;
			EditorGUI.PropertyField(
				new Rect(rect.x, rect.y, 30, EditorGUIUtility.singleLineHeight),
				element.FindPropertyRelative("spawnTime"), GUIContent.none);
			EditorGUI.PropertyField(
				new Rect(rect.x + 35, rect.y, 70, EditorGUIUtility.singleLineHeight),
				element.FindPropertyRelative("enemyType"), GUIContent.none);
			EditorGUI.PropertyField(
				new Rect(rect.x + 110, rect.y, 30, EditorGUIUtility.singleLineHeight),
				element.FindPropertyRelative("enemyCount"), GUIContent.none);
			EditorGUI.PropertyField(
				new Rect(rect.x + 110 + 35, rect.y, 40, EditorGUIUtility.singleLineHeight),
				element.FindPropertyRelative("spawnPoint"), GUIContent.none);
		};
	}

	public override void OnInspectorGUI () {
		serializedObject.Update();
		list.DoLayoutList();
		serializedObject.ApplyModifiedProperties();
	}
}
