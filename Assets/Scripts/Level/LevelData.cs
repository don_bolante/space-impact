using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelData : ScriptableObject {

	public List<Level> list = new List<Level>();
}
