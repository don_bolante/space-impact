﻿using UnityEngine;
using System.Collections;

public class LevelController : SpaceImpactElement {

	[SerializeField] LevelData level1;

	public override void Initialize () {
		App.controller.level = this;
		StartCoroutine(SpawnWaves());
	}

	IEnumerator SpawnWaves ()
	{
		int index = 0;
		int enemyId = 0;
		yield return new WaitForSeconds (level1.list[index].spawnTime);
		while (true)
		{
			for (int i = 0; i < level1.list[index].enemyCount; i++)
			{
				App.controller.enemyCache.CreateEnemy(enemyId, level1.list[index].enemyType);
				App.model.enemyList[enemyId].position = new Vector3(Mathf.Abs(StaticHelper.GetScreenBoundaries().x) - .5f, level1.list[index].spawnPoint, 0);
				App.controller.enemyList[enemyId].isActive = true;
//				Debug.Log("Spawn " + level1.list[index].enemyType.ToString());
				enemyId ++;
				yield return new WaitForSeconds (1f);
			}
			index++;
			if(index >= level1.list.Count) {
				return false;
			}
			yield return new WaitForSeconds (level1.list[index].spawnTime);
		}
	}
}
