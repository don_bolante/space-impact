﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Level {

	public int spawnTime;
	public EnemyType enemyType;
	public int enemyCount;

	/// <summary>
	/// The spawn point, in Y-axis.
	/// </summary>
	public float spawnPoint;
}
