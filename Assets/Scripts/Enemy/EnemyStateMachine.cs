﻿using UnityEngine;
using System.Collections;

public class EnemyStateMachine : SpaceImpactElement {

	EnemyController enemyController;

	[SerializeField] Vector3[] nodes;
	int nodeIndex;

	[SerializeField] Vector3[] nodes2;
	int node2Index;

	[SerializeField] Vector3[] bossNodes;
	int bossNodeIndex;

	public override void Initialize () {

	}

	void Start () {
		enemyController = GetComponent<EnemyController>();
	}

	public override void Update () {
		if(!enemyController.isActive) return;

		EnemyModel enemyModel = enemyController.App.model.enemyList[enemyController.id];

		switch(enemyModel.state) {
		case EnemyState.Moving:
			enemyModel.position += Vector3.left * Time.deltaTime;
			break;
		case EnemyState.MoveOnAPath:
			if(nodeIndex.Equals(nodes.Length)) return;
			enemyModel.position = Vector3.MoveTowards(enemyModel.position, nodes[nodeIndex], Time.deltaTime);
			if(enemyModel.position == nodes[nodeIndex]) {
				nodeIndex ++;
			}
			break;
		case EnemyState.MoveOnAPath2:
			if(node2Index.Equals(nodes2.Length)) return;
			enemyModel.position = Vector3.MoveTowards(enemyModel.position, nodes2[node2Index], Time.deltaTime);
			if(enemyModel.position == nodes2[node2Index]) {
				node2Index ++;
			}
			break;
		case EnemyState.Custom:
			enemyModel.position = Vector3.MoveTowards(enemyModel.position, bossNodes[bossNodeIndex], Time.deltaTime);
			if(enemyModel.position == bossNodes[bossNodeIndex]) {
				bossNodeIndex ++;
				if(bossNodeIndex >= bossNodes.Length) {
					bossNodeIndex = 0;
				}
			}
			break;
		}
		
		if(!StaticHelper.IsVisible(enemyController.App.view.enemyList[enemyController.id].transform)) {
			enemyController.SetActive(false);
		}
	}

	public void ChangeState(EnemyState state) {
		enemyController.App.model.enemyList[enemyController.id].state = state;
	}
}
