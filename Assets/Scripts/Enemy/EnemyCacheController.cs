﻿using UnityEngine;
using System.Collections;

public class EnemyCacheController : SpaceImpactElement {

	[SerializeField] EnemyView enemyPrefab1;
	[SerializeField] EnemyController enemyControllerPrefab;
	[SerializeField] int count;

	#region Game Design
	[SerializeField] Sprite[] enemySprites;
//	[SerializeField] LevelData level1;
	#endregion

	public override void Initialize () {
		App.controller.enemyCache = this;

	}

	public void CreateEnemy (int id, EnemyType type) {
		App.model.enemyList.Add(new EnemyModel(id, type));

		EnemyView enemyView = Instantiate(enemyPrefab1) as EnemyView;
		App.view.enemyList.Add(enemyView);
		enemyView.transform.SetParent(App.view.enemyCache.transform);
		enemyView.id = id;
		SpriteRenderer spriteRenderer = enemyView.GetComponent<SpriteRenderer>();
		spriteRenderer.sprite = enemySprites[(int)type];
		BoxCollider2D collider2D = enemyView.GetComponent<BoxCollider2D>();
		collider2D.size = new Vector2(spriteRenderer.bounds.size.x, spriteRenderer.bounds.size.y);

		EnemyController enemyController = Instantiate(enemyControllerPrefab) as EnemyController;
		App.controller.enemyList.Add(enemyController);
		enemyController.transform.SetParent(App.controller.enemyCache.transform);
		enemyController.id = id;
	}

	public void RemoveEnemy (int id) {
		App.controller.enemyList[id].SetActive(false);
	}
}
