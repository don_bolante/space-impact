﻿using UnityEngine;
using System.Collections;

public class EnemyController : SpaceImpactElement {

	public int id;
	public bool isActive;

	public override void Initialize () {
//		App.controller.enemyList.Add(this);
	}

	public void Notify (string command, GameObject agent) {
		switch(command) {
		case Constants.Hit:
			switch(agent.tag) {
			case Constants.Hero:
			case Constants.Player:
				if(App.model.enemyList[id].type == EnemyType.Boss) {

				}
				else {
					App.controller.enemyCache.RemoveEnemy(id);
					App.model.score += 10 * ((int)App.model.enemyList[id].type + 1);
				}
				break;
			case Constants.Ammo:
				if(App.model.enemyList[id].type == EnemyType.Boss) {
					if((App.model.enemyList[id].health - App.model.hero.damage) <= 0) {
						App.controller.enemyCache.RemoveEnemy(id);
						App.model.score += 10 * ((int)App.model.enemyList[id].type + 1);

						App.controller.Notify(Constants.GameOver);
					}
					else {
						App.model.enemyList[id].health -= App.model.hero.damage;
						App.model.score += 5;
					}
				}
				else {
					if((App.model.enemyList[id].health - App.model.hero.damage) <= 0) {
						App.controller.enemyCache.RemoveEnemy(id);
						App.model.score += 10 * ((int)App.model.enemyList[id].type + 1);
					}
					else {
						App.model.enemyList[id].health -= App.model.hero.damage;
						App.model.score += 5;
					}
				}
				break;
			default:
				// Do nothing.
				break;
			}
			App.controller.hud.Notify(Constants.Update);
			break;
		}
	}

	public void SetActive (bool flag) {
		if(flag) {
			App.model.enemyList[id].position = Vector3.zero;
			isActive = true; 
		}
		else {
			App.model.enemyList[id].position = Vector3.one * 500;
			isActive = false;
		}
	}
}
