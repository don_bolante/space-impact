﻿using UnityEngine;
using System.Collections;

public class EnemyView : SpaceImpactElement {

	public int id;

	public override void Initialize () {
//		App.view.enemyList.Add(this);
	}

	public override void Update () {
		transform.position = App.model.enemyList[id].position;
	}

	void OnCollisionEnter2D (Collision2D collision) {
		if(!App.controller.enemyList[id].isActive) return;
		App.controller.enemyList[id].Notify(Constants.Hit, collision.gameObject);
	}
}
