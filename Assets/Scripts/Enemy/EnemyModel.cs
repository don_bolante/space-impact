﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class EnemyModel {

	public int id;

	public EnemyState state;
	public EnemyType type;

	public int health;

	public Vector3 position;

	public EnemyModel (int id, EnemyType type) {
		this.id = id;
		this.type = type;
		position = Vector3.one * 5000;

		switch(type) {
		case EnemyType.Level1:
			health = 2;
			state = EnemyState.MoveOnAPath2;
			break;
		case EnemyType.Level2:
			health = 1;
//			state = EnemyState.Moving;
			state = EnemyState.MoveOnAPath;
			break;
		case EnemyType.Level3:
			state = EnemyState.Moving;
			health = 2;
			break;
		case EnemyType.Level4:
			state = EnemyState.MoveOnAPath;
			health = 3;
			break;
		case EnemyType.Level5:
			state = EnemyState.MoveOnAPath2;
			health = 5;
			break;
		case EnemyType.Boss:
			state = EnemyState.Custom;
			health = 50;
			break;
		}
	}
}
