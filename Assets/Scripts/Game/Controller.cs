﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

	SpriteRenderer spriteRenderer;
	public Stats stats;

	public bool isActive;
	[SerializeField] public float moveSpeed;

	public virtual void Start () {

	}

	public virtual void Update () {
		if(!isActive) {
			return;
		}
		transform.position += Vector3.right * moveSpeed * Time.deltaTime;
		
		if(!StaticHelper.IsVisible(transform)) {
			SetActive(false);
		}
	}

	public virtual void SetActive (bool flag) {
		isActive = flag;
		if(flag) {
			
		}
		else {
			transform.position = Vector3.left * 10000;
		}
	}

	public void EventSendDamage () {
		stats.AddRemoveHealth(-1);
		if(stats.IsDead()) {
			SetActive(false);
		}
	}
}