using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpaceImpactController : MonoBehaviour {

	public LevelController level;

	public HeroController hero;
	public HeroStateMachine heroStateMachine;

	public AmmoCacheController ammoCache;
	public List<AmmoController> ammoList;

	public EnemyCacheController enemyCache;
	public List<EnemyController> enemyList;

	public PickupController pickup;

	public HUDController hud;

	public MainMenuController mainMenu;
	public GameOverController gameOver;
	public HighScoreController highScore;

	void Awake () {
		SpaceImpactApplication app = GameObject.FindObjectOfType<SpaceImpactApplication>();
		app.controller = this;
	}

	public void Notify (string command) {
		SpaceImpactApplication App = GameObject.FindObjectOfType<SpaceImpactApplication>();
		if(command == Constants.GameOver) {
			if(App.model.score > App.model.highScore) {
				App.model.highScore = App.model.score;
			}
			App.model.lastScore = App.model.score;
			Application.LoadLevel(Constants.GameOver);
		}
	}
}
