﻿using UnityEngine;
using System.Collections;

public class SpaceImpactApplication : MonoBehaviour {

	public static SpaceImpactApplication Instance { private set; get; }

	public SpaceImpactModel model;
	public SpaceImpactView view;
	public SpaceImpactController controller;

	void Awake () {
		Instance = this;
	}
}
