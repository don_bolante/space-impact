﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollisionManager : MonoBehaviour {

	public static CollisionManager Instance { private set; get; }
	private List<SpriteCollider> colliders = new List<SpriteCollider>();

	void Awake () {
		if(Instance == null) {
			Instance = this;
		}
		else {
			Destroy(Instance);
			Instance = this;
		}
	}

	public void Update () {

	}

	public void Register (SpriteCollider spriteCollider) {
		colliders.Add(spriteCollider);
	}
}
