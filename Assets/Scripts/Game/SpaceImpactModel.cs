﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpaceImpactModel : MonoBehaviour {

	#region Game-related models
	public float timeElapsed;

	private int _highScore;
	public int highScore {
		get {
			if(PlayerPrefs.HasKey(Constants.HighScore)) {
				_highScore = PlayerPrefs.GetInt(Constants.HighScore);
			}
			return _highScore; 
		}
		set { 
			_highScore = value;
			PlayerPrefs.SetInt(Constants.HighScore, value);
		}
	}

	public int score;

	private int _lastScore;
	public int lastScore {
		get {
			if(PlayerPrefs.HasKey(Constants.LastScore)) {
				_lastScore = PlayerPrefs.GetInt(Constants.LastScore);
			}
			return _lastScore;
		}
		set {
			_lastScore = value;
			PlayerPrefs.SetInt(Constants.LastScore, value);
		}
	}

	public int lives;
	public int powerup;
	public int powerupCount;
	#endregion

	public HeroModel hero = new HeroModel();
	public AmmoCacheModel ammoCache = new AmmoCacheModel();
	public List<AmmoModel> ammoList = new List<AmmoModel>();

	public List<EnemyModel> enemyList = new List<EnemyModel>();

	public PickupModel pickup = new PickupModel();
	
	void Awake () {
		SpaceImpactApplication app = GameObject.FindObjectOfType<SpaceImpactApplication>();
		app.model = this;
	}

	void Update () {
		timeElapsed += Time.deltaTime;
	}
}
