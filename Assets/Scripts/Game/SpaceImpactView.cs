﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpaceImpactView : MonoBehaviour {

	public HeroView hero;

	public AmmoCacheView ammoCache;
	public List<AmmoView> ammoList;

	public EnemyCacheView enemyCache;
	public List<EnemyView> enemyList;

	public PickupView pickup;

	public HUDView hud;

	public MainMenuView mainMenu;
	public GameOverView gameOver;
	public HighScoreView highScore;

	void Awake () {
		SpaceImpactApplication app = GameObject.FindObjectOfType<SpaceImpactApplication>();
		app.view = this;
	}
}
