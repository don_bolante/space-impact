﻿using UnityEngine;
using System.Collections;

public class SpaceImpactElement : MonoBehaviour {

	[HideInInspector]
	public SpaceImpactApplication App;

	void Awake () {
		App = GameObject.FindObjectOfType<SpaceImpactApplication>();
	}

	void Start () {
		Initialize();
	}

	public virtual void Initialize () {
		Debug.Log("Something is not yet initialized.");
	}

	public virtual void Update () {
		return;
	}
}
