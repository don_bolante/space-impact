﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Stats {

	public Stats () {}
	public Stats (int health, int damage) {
		_health = health;
		_damage = damage;
	}

	private int _health;
	public int health {
		set { _health = value; }
		get { return _health; }
	}

	private int _damage;
	public int damage {
		set { _damage = value; }
		get { return _damage; }
	}

	public void AddRemoveHealth (int amount) {
		_health += amount;
	}

	public bool IsDead () {
		return health <= 0;
	}
}
