﻿using UnityEngine;
using System.Collections;

public class Enums {


}

public enum HeroState {
	None = 0,
	Invulnerable
}

public enum PowerupType {
	None = -1,
	Laser = 0,
	Missile,
	Shield
}

public enum EnemyType {
	Level1 = 0,
	Level2,
	Level3,
	Level4,
	Level5,
	Boss
}

public enum EnemyState {
	None = 0,
	Moving,
	MoveOnAPath,
	MoveOnAPath2,
	Custom
}