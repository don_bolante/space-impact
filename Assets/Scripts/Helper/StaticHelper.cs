﻿using UnityEngine;
using System.Collections;

public class StaticHelper {

	public static bool IsVisible (Transform transform) {
		return transform.position.x > GetRestrictedPosition(transform).x && transform.position.x < -GetRestrictedPosition(transform).x;
	}

	public static Vector2 GetRestrictedPosition (Transform transform) {
		return new Vector2(GetScreenBoundaries().x + (GetSpriteSize(transform).x * 0.5f), GetScreenBoundaries().y + (GetSpriteSize(transform).y * 0.5f));
	}

	public static Vector2 GetScreenBoundaries () {
		Vector3 screenToWorldPoint = Camera.main.ScreenToWorldPoint(Vector3.zero);
		return new Vector2(screenToWorldPoint.x, screenToWorldPoint.y);
	}
	
	public static Vector2 GetSpriteSize (Transform transform) {
		float offset = -1f;
		SpriteRenderer spriteRenderer = transform.GetComponentInChildren<SpriteRenderer>();
		Bounds bounds = spriteRenderer.bounds;
		return new Vector2(bounds.size.x + offset, bounds.size.y + offset);
	}
}
