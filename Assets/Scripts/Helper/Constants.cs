﻿using UnityEngine;
using System.Collections;

public class Constants {

	#region Tags
	public const string Hero = "Hero";
	public const string Player = "Player";
	public const string Enemy = "Enemy";
	public const string Ammo = "Ammo";
	public const string Powerup = "Powerup";
	#endregion

	public const string RefreshUI = "EventRefreshUI";

	public const string Hit = "Hit";
	public const string Update = "Update";

	#region Scene names and items
	public const string MainMenu = "MainMenu";
	public const string Game = "Game";
	public const string GameOver = "GameOver";
	public const string HighScore = "HighScore";
	public const string NewGame = "NewGame";
	public const string Exit = "Exit";
	public const string LastScore = "CurrentScore";
	#endregion
}
