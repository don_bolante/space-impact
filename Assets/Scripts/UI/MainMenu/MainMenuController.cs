﻿using UnityEngine;
using System.Collections;

public class MainMenuController : SpaceImpactElement {
	
	public override void Initialize () {
		App.controller.mainMenu = this;
	}

	public void Notify (string command) {
		switch(command) {
		case Constants.NewGame:
			Application.LoadLevel(Constants.Game);
			break;
		case Constants.HighScore:
			Application.LoadLevel(Constants.HighScore);
			break;
		case Constants.Exit:
//			Application.LoadLevel(Constants.Exit);
			Application.Quit();
			break;
		}
	}
}
