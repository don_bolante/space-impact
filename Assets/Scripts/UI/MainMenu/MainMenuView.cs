﻿using UnityEngine;
using System.Collections;

public class MainMenuView : SpaceImpactElement {
	
	public override void Initialize () {
		App.view.mainMenu = this;
	}

	public void OnClick (string command) {
		App.controller.mainMenu.Notify(command);
	}
}
