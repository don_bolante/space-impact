﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDView : SpaceImpactElement {
	
	[SerializeField] public Text score;
	[SerializeField] public Image[] lives;
	[SerializeField] public Image powerup;
	[SerializeField] public Text powerupCount;

//	void OnEnable () {
//		Messenger.AddListener(Constants.RefreshUI, Refresh);
//	}
//
//	void OnDisable () {
//		Messenger.RemoveListener(Constants.RefreshUI, Refresh);
//	}

	public override void Initialize () {
		App.view.hud = this;
	}

	void Refresh () {
//		App.controller.hud.Notify("Update");
	}
}
