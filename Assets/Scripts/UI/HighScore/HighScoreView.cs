﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HighScoreView : SpaceImpactElement {

	[SerializeField] public Text scoreText;

	public override void Initialize () {
		App.view.highScore = this;
	}

	public override void Update () {
		if(Input.GetKeyUp(KeyCode.Escape)) {
			Application.LoadLevel(Constants.MainMenu);
		}
	}
}
