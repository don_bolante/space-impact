﻿using UnityEngine;
using System.Collections;

public class HighScoreController : SpaceImpactElement {

	public override void Initialize () {
		App.controller.highScore = this;
	}

	void Start () {
		Notify(Constants.Update);
	}

	public void Notify (string command) {
		switch(command) {
		case Constants.Update:
			App.view.highScore.scoreText.text = App.model.highScore.ToString().PadLeft(5, '0');
			break;
		default:
			// Do nothing.
			break;
		}
	}
}
