﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverView : SpaceImpactElement {

	[SerializeField] public Text scoreText;

	public override void Initialize () {
		App.view.gameOver = this;
	}

	public override void Update () {
		if(Input.GetKeyUp(KeyCode.Escape)) {
			Application.LoadLevel(Constants.MainMenu);
		}
	}
}
