﻿using UnityEngine;
using System.Collections;

public class GameOverController : SpaceImpactElement {
	
	public override void Initialize () {
		App.controller.gameOver = this;
	}

	void Start () {
		Notify(Constants.Update);
	}

	public void Notify (string command) {
		switch(command) {
		case Constants.Update: 
			App.view.gameOver.scoreText.text = App.model.lastScore.ToString().PadLeft(5, '0');
			break;
		default:
			// Do nothing.
			break;
		}
	}
}
