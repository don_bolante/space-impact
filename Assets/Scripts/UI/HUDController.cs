﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDController : SpaceImpactElement {

	public override void Initialize () {
		App.controller.hud = this;
//		Messenger.Broadcast(Constants.RefreshUI);
	}

	public void Notify (string command) {
		switch(command) {
		case Constants.Update:
			// Update score
			App.view.hud.score.text = App.model.score.ToString().PadLeft(5, '0');

			// Update lives
			for(int i = 0; i < App.view.hud.lives.Length; i++) {
				App.view.hud.lives[i].gameObject.SetActive(false);
			}

			for(int i = 0; i < App.model.lives; i++) {
				App.view.hud.lives[i].gameObject.SetActive(true);
			}

			// Update powerup
			App.view.hud.powerupCount.text = App.model.powerupCount.ToString().PadLeft(2, '0');
			// add powerup icon
			break;
		}
	}
}
