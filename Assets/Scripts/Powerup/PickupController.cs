﻿using UnityEngine;
using System.Collections;

public class PickupController : SpaceImpactElement {

	public bool isActive;

	public override void Initialize () {
		App.controller.pickup = this;
		isActive = true;
	}

	public override void Update () {
		if(!isActive) return;

		App.model.pickup.position += Vector3.left * Time.deltaTime;
	}

	public void Notify (string command, GameObject agent) {
		switch(command) {
		case Constants.Hit:
			switch(agent.tag) {
			case Constants.Player:
				SetActive(false);
				break;
			default:
				// Do nothing.
				break;
			}
			break;
		}
	}

	void SetActive (bool flag) {
		if(flag) {
			App.model.pickup.position = new Vector3(Mathf.Abs(StaticHelper.GetScreenBoundaries().x) - .5f,
			                                        Random.Range(StaticHelper.GetScreenBoundaries().y, -StaticHelper.GetScreenBoundaries().y));
			isActive = true;
		}
		else {
			App.model.pickup.position = Vector3.one * 5000f;
			isActive = false;
		}
	}
}
