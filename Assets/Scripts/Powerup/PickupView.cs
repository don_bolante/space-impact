﻿using UnityEngine;
using System.Collections;

public class PickupView : SpaceImpactElement {

	public override void Initialize () {
		App.view.pickup = this;
	}

	public override	void Update () {
		transform.position = App.model.pickup.position;
	}

	void OnCollisionEnter2D(Collision2D collision) {
		App.controller.pickup.Notify(Constants.Hit, collision.gameObject);
	}
}
